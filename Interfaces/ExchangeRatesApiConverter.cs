﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class ExchangeRatesApiConverter : ICurrencyConverter
    {

        private HttpClient _httpClient;
        private IMemoryCache _memoryCache;
        private string _apiKey;

        private string _url;
        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _url = "http://api.exchangeratesapi.io/v1/latest?access_key=" + apiKey;
        }


        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            var jsonString = _memoryCache.GetOrCreateAsync("exchangeRate", async entry =>
                                        {
                                            entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(20);
                                            var exchangeAnswer = await _httpClient.GetAsync(_url);
                                            string body = await exchangeAnswer.Content.ReadAsStringAsync();
                                            return body;
                                        });

            jsonString.Wait();

            ExchangeRates exchangeRates = ExchangeRates.FromJson(jsonString.Result);

            var baseAmount = ConvertToBaseCurrency(amount, exchangeRates);

            return  (ICurrencyAmount)ConvertFromBaseCurrency(baseAmount.Amount, currencyCode, exchangeRates);

        }


        private CurrencyAmount ConvertToBaseCurrency(ICurrencyAmount amount, ExchangeRates exchangeRates)
        {            
            double rate;

            if (exchangeRates.Rates.TryGetValue(amount.CurrencyCode, out rate))
            {
                CurrencyAmount retAmount = new CurrencyAmount(exchangeRates.Base, amount.Amount / (decimal)rate);

                return retAmount;
            }
            else
            {
                throw new InvalidOperationException($"{exchangeRates.Base} currency not support by https://exchangeratesapi.io/");
            }            
        }

        private CurrencyAmount ConvertFromBaseCurrency(decimal baseAmount, string newCurrencyCode, ExchangeRates exchangeRates)
        {
            double rate;

            if (exchangeRates.Rates.TryGetValue(newCurrencyCode, out rate))
            {
                CurrencyAmount retAmount = new CurrencyAmount(newCurrencyCode, baseAmount * (decimal)rate);

                return retAmount;
            }
            else
            {
                throw new InvalidOperationException($"{exchangeRates.Base} currency not support by https://exchangeratesapi.io/");
            }
        }
    }
}
