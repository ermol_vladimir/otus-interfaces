﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Interfaces
{
    class FileTransactionRepository : ITransactionRepository
    {
        const string fileName = @"../../../BudgetApp.txt";

        private const string _expense_code = "Трата";
        private const string _income_code = "Доход";
        private const string _transfer_code = "Перевод";

        private const string _splitter = " ";

        public void AddTransaction(ITransaction transaction)
        {
            using(StreamWriter sw = new StreamWriter(fileName, true))
            {
                sw.WriteLine(CreateString(transaction));
            }
        }

        public ITransaction[] GetTransactions()
        {
            List<ITransaction> transactions = new List<ITransaction>();

            using (StreamReader sr = new StreamReader(fileName))
            {
                while(!sr.EndOfStream)
                {
                    transactions.Add(ParceString(sr.ReadLine()));
                }
            }
            return transactions.ToArray();
        }


        
        public ITransaction ParceString(string input)
        {
            var splits = input.Split(new[] { _splitter }, StringSplitOptions.RemoveEmptyEntries);
            var typeCode = splits[0];
            var currencyAmount = new CurrencyAmount(splits[2], decimal.Parse(splits[1]));
            switch (typeCode)
            {
                case _expense_code:
                    return new Expense(currencyAmount, DateTimeOffset.Parse(splits[5]), splits[3], splits[4]);
                case _income_code:
                    return new Income(currencyAmount, DateTimeOffset.Parse(splits[4]), splits[3]);
                case _transfer_code:
                    return new Transfer(currencyAmount, DateTimeOffset.Parse(splits[5]), splits[3], splits[4]);
                default:
                    throw new NotImplementedException();
            }
        }

        public string CreateString(ITransaction input)
        {

            switch (input.GetType().Name)
            {
                case (nameof(Expense)):
                    Expense expense = input as Expense;
                    return _expense_code + _splitter + expense.Amount.Amount.ToString() + _splitter + expense.Amount.CurrencyCode + _splitter + expense.Category + _splitter + expense.Destination + _splitter + expense.Date;
                case (nameof(Income)):
                    Income income = input as Income;
                    return _expense_code + _splitter + income.Amount.Amount.ToString() + _splitter + income.Amount.CurrencyCode + _splitter + income.Source + _splitter + income.Date;
                case (nameof(Transfer)):
                    Transfer transfer = input as Transfer;
                    return _expense_code + _splitter + transfer.Amount.Amount.ToString() + _splitter + transfer.Amount.CurrencyCode + _splitter + transfer.Destination + _splitter + transfer.Message + _splitter + transfer.Date;
                default:
                    throw new NotImplementedException();
            }
        }

    }
}

