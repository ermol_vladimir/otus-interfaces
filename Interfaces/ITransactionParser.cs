﻿namespace Interfaces
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
