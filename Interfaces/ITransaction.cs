﻿using System;

namespace Interfaces
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }
        ICurrencyAmount Amount { get; }                
    }
}
