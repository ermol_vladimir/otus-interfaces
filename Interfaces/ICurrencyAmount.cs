﻿using System;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get; }
        decimal Amount { get; }
    }
}
